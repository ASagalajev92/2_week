﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidEdasi
{
    class Person
    {
        static int loendur = 0; //staatiline vali - see on klassil mitte objektil 
        public static int Count => loendur; // staatiline property - readonly

        public static List<Person> People = new List<Person>();

        public string FirstName; // mittestaatilised valjad - objekti valjad
        public string LastName;
        public string PID;      // syymmddnnnx  s - sugu ja sajand  .Substring(0,1)
                                //              yy - aasta          .Substring(1,2)
                                //              mm - kuu            .Substring(3,2)
                                //              dd - päev           .Substring(5,2)


        //variant 1
        //public readonly int Nr = ++loendur; // private vali ja readonly property

        //variant 2
        //private int _Nr => ++loendur; 
        //public int Nr => _Nr; // readonly private property

        //variant 3
        //public int {get;} = ++loendur; // public readonly property



        #region Meetodite variandid

        //public Person Mother; // default is null 
        //public Person Father; 
        //public int ShoeNumber; // default is 0 
        //public List<Person> Children = new List<Person>();  

        #endregion


        public Gender Gender => (Gender)(PID[0] % 2);  // kiire ja elegantne aga raske lugeda ja aru saada


        public String FullName => $"{FirstName} {LastName}";  // siia mõtle miskit välja

        public DateTime DayOfBirth // kiirem ja lihtsam aga keerulisem kirjutada
        => new DateTime(
                ((PID[0] - '1') / 2 + 18) * 100 +
                int.Parse(PID.Substring(1, 2)),
                int.Parse(PID.Substring(3, 2)),
                int.Parse(PID.Substring(5, 2))
                );

        public override string ToString() => $"{Gender}: {FullName} ({PID})";

        public int Age
            => (DateTime.Today - DayOfBirth).Days * 4 / 1461; //siia nulli asemele siis avaldis , mis annaks vanuse

        public string FirstName
        {
            get => _FirstName;
            set => _FirstName = value.ToProper(); // _FirstName konvertirujet v bolwoje
        }


        

    }
}
