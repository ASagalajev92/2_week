﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace _2_week
{
    class Program
    {
        static void Main(string[] args)

        {
            Inimene sass = new Inimene();

            sass.Eesnimi = Console.ReadLine();
            Console.WriteLine(sass.Eesnimi);




            //Person mina = new Person { FirstName = (name), LastName = name2, ISIC = number1 }; // oomistab andmed
            //Console.WriteLine(mina.FirstName); // vottab teisest failist nime
            //Console.WriteLine(mina.DayOfBirdth); // vottab teisest failist sunnipaeva
            //Console.WriteLine(mina.Gender); // vottab teisest failist tehingu suguga ja trukkib selle tulemuse consoolil



            #region Iseseisev praktika(natukene massiividega)
            //// Iseseisev praktika natukene 
            //Random r = new Random();

            //int [] arvud = new int [] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            //for (int i = 0; i < arvud.Length ; i++)
            //{
            //    Console.Write($" Arv : {arvud[i]}");
            //    if (arvud.Length > 10) break;
            //} 
            #endregion

            #region Ulesanne 1

            //    Random r = new Random(); 

            //    List<int> kaardid = Enumerable.Range(0, 52).ToList();

            //    for (int i = 0; i < 52;) 
            //    {
            //        Console.WriteLine($"\t{kaardid[i]}{(++i % 13 == 0 ? "\n" : "")}:");
            //    }



            //    List<int> segatud = new List<int>();    /// siin pole uhtegi kaardi

            //    while (kaardid.Count > 0 )
            //    {
            //        int mitmes = r.Next(kaardid.Count); //vottame juhuslikku kaardi
            //        segatud.Add(kaardid[mitmes]);       // paneme selle kaardi teise pakki
            //        kaardid.RemoveAt(mitmes);           // korjame ta esimesest pakkist ara

            //        //int kaart = kaardid[mitmes];        // see on see, mille umber tostetakse
            //        //kaardid.Remove(kaart);            // see on inim
            //        //segatud.Add(kaart);
            //    }

            //    for (int i = 0; i < 52; )
            //    {
            //           Console.WriteLine($"\t{segatud[i]}{(++i % 13 == 0 ? "\n" : "")}:");

            //Random r = new Random();

            //List<int> vanused = Enumerable.Range(0, 21).ToList();

            //for (int i = 1; i < 21; )
            //{
            //    Console.Write($"\t{vanused[i]}{(++i % 13 == 0 ? "\n" : "")}:");
            //}

            //List<int> segatud = new List<int>();

            //while (vanused.Count > 0)
            //{
            //    int suvaline = r.Next(vanused.Count);
            //    segatud.Add(vanused[suvaline]);

            //}



            #endregion

            #region Harjutus 2
            //string failiasukoht = @"..\..\fail.txt"; // faili asukoht
            //string[] loetudRead = File.ReadAllLines(failiasukoht); //
            //foreach (var r in loetudRead)
            //{
            //    Console.WriteLine(r);
            //}
            //List<String> nimed = new List<string>();
            //List<int> distantsid = new List<int>();
            //List<int> ajad = new List<int>();

            //List<double> kiirused = new List<double>();

            //for (int i = 1; i < loetudRead.Length ; i++)
            //{
            //    string rida = loetudRead[i]; // see on uks rida
            //    string[] osad = rida.Split(','); // tukeldatud rida
            //    nimed.Add(osad[0]);
            //    distantsid.Add(int.Parse(osad[1].Trim()));
            //    ajad.Add(int.Parse(osad[2].Trim()));
            //    kiirused.Add(distantsid[i - 1] * 0.1 / ajad[i - 1]);
            //}

            //Double kiireim = kiirused.Max();
            //for (int i = 0; i < nimed.Count; i++)
            //{
            //    if (kiirused[i] == kiireim)
            //    {
            //        Console.WriteLine($"Kiirem oli {nimed[i]} tema kiirus oli {kiirused[i]}");
            //    }
            //} 
            #endregion

            #region Class practice OOP
            //Console.WriteLine("Tere inimene !");

            //Inimene aleksandr = new Inimene
            //{
            //    Eesnimi = "Aleksand ",
            //    Perenimi = "Sagalajev",

            //    Vanus = 40
            //};

            //Inimene karl = new Inimene
            //{
            //    Eesnimi = "Karl",
            //    Perenimi = "Taukar",

            //    Vanus = 59
            //};

            //Console.WriteLine(aleksandr);
            //Console.WriteLine(karl);

            //List<Inimene> inimesed = new List<Inimene>();
            //inimesed.Add(aleksandr);
            //inimesed.Add(karl);
            //inimesed.Add(new Inimene { Eesnimi = "Jaak", Vanus = 34 });

            //string nimekiri = "";
            //foreach (var x in inimesed)
            //{
            //    nimekiri += x.
            //}

            //foreach (var x in inimesed)
            //{
            //    Console.WriteLine($"inimese nimi on {x.()}";
            //} 
            #endregion

            #region Naidis muutujate erinevustest

            //Inimene sarvik = aleksandr;

            //int a = 7;
            //int b = a;
            //b = 8;
            //Console.WriteLine(a);  //tavaline muutuja teeb nii

            //sarvik.Nimi = "Sarvik";
            //Console.WriteLine(aleksandr.Nimi);

            #endregion

            #region Vozmoznosti classov


            Onlyclass m; // nazna4at peremennije 

            List<Onlyclass> list; // rabotat so spiskami

            Onlyclass[] randomnumbers; // rabotat s massivami 

            Practicclass x = new Practicclass(); // Prisvaivaet peremennuju x klassu Practicclass i zaprawivajet mesto pod etu
            // peremennuju v pamjati sistemq(4em bolwe class tem bolwe pamjati budet vodelenno na nego)

            x.A = 7; // prisvojenije public peremennoi v classe zna4enije 7 

            Practicclass j = new Practicclass() { A = 12 }; // prisvojenije peremennoi j i prisvojenije zna4enije peremennoi v classe
                                                            // A so zna4enijem 12
            #endregion

            #region ENUM Training |=,^=,=
            Pol p = Pol.Devuwka; // prisvojenije enum Pol peremennoi i vobor iz ENUMA Devuwku
            Console.WriteLine(p); // vqvod Devuwka
            Console.WriteLine((int)p);

            Console.WriteLine(DayOfWeek.Monday);

            Kirjeldus k = Kirjeldus.Kolisec | Kirjeldus.Punanen; // lisame kolisec ja punane koos koma vahel
            Console.WriteLine(k);
            k |= Kirjeldus.Puust; // Lisame juurde Puust 
            Console.WriteLine(k);
            k ^= Kirjeldus.Puust; // Eemaldasime Puust
            Console.WriteLine(k);
            k = (Kirjeldus)13; // Iwet est li v ENUM podhodjawije 4islitelnije prisvojennije nazvanijam i pe4atajet vse ih po porjadku
            Console.WriteLine(k); // Puust , Suur , Kolisev 
            #endregion



            
        }

        public


    }

    #region Pustoi class ( Onlyclass )


    class Onlyclass { }


    #endregion

    #region Rabo4ij class (Practicclass)
    class Practicclass
    {
        public int A;
        int B;
    }

    enum Pol { Devuwka, Paren } // nado pisat Female sna4alo 

    enum Type { Kresti, Tservi, Bubi, Piki } // Karto4nije masti 

    [Flags] enum Kirjeldus { Puust = 1, Punanen = 2, Suur = 4, Kolisec = 8 }
    // [Flags] pozvoljajet  ENUM spisku prisvaivat 4islovije zna4enija
    #endregion

    #region Class programmi sees

    //struct Inimene
    //{
    //    public string Nimi;
    //    public int Vanus; // MILLEST KOSNEB

    //    public override string ToString() => $"{Nimi} vanusega {Vanus}"; // MIDA OSKAB

    //}

    #endregion

}
