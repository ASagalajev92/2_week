﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_week
{
    class Inimene
    {
        public string Eesnimi;
        public string Perenimi; // MILLEST KOSNEB
        public int Vanus;       // MILLEST KOSNEB

        public override string ToString() => $"{AllName()} vanusega {Vanus}"; // MIDA OSKAB

        public string AllName()
        {
            return Eesnimi + " " + Perenimi;
        }

    }
}
